import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    blocks: [
      {
        id: 1,
        name: 'silver',
        color: '#c0c0c0',
      },
      {
        id: 2,
        name: 'steelblue',
        color: '#4983b2',
      },
      {
        id: 3,
        name: 'slategrey',
        color: '#71808f',
      },
      {
        id: 4,
        name: 'skyblue',
        color: '#89ceea',
      },
    ],
  },
  actions: {},
  mutations: {
    moveblocks(state) {
      const blocks = state.blocks;
      blocks.unshift(blocks[3]);
      blocks.pop();
      state.blocks = blocks;
    },
  },
  getters: {
    blocks: state => state.blocks,
  },
});

export default store;
